import { createContext } from "react";
import {
  defaultMqttClient,
  defaultSelectionStorage,
  defaultV2xStorage,
  useAppStorage,
  UseAppStoreHook,
} from "../hooks";

interface IAppStorageContext extends UseAppStoreHook {}

const defaultAppContext: IAppStorageContext = {
  apiKey: "",
  setApiKey: () => "",

  realtimeFlag: false,
  setRealtimeFlag: () => true,

  positionsFlag: false,
  setPositionsFlag: () => true,

  realtime: {
    v2xStorage: defaultV2xStorage,
    mqttClientStorage: defaultMqttClient,
    selectionStorage: defaultSelectionStorage,
  },
};

export const AppStorageContext = createContext(defaultAppContext);

type Props = {
  children: React.ReactNode;
};

export const AppStorageProvider = (props: Props) => {
  const { children } = props;
  const hook = useAppStorage();

  return (
    <AppStorageContext.Provider value={hook}>
      {children}
    </AppStorageContext.Provider>
  );
};
