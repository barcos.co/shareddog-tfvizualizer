import React, { useContext, useEffect } from "react";
import { AppStorageContext } from "./contexts/appStorage";
import { TLPositions, TLSelector } from "./views";
import "./App.css";
import TLRealtimeStatus from "./views/TLRealtimeStatus";

const apiKeyValue = process.env.REACT_APP_GOOGLE_API ?? "";

export const App = () => {
  const {
    realtimeFlag,
    setRealtimeFlag,
    positionsFlag,
    setPositionsFlag,
    setApiKey,
  } = useContext(AppStorageContext);

  useEffect(() => {
    setApiKey(apiKeyValue);
  }, []);

  return (
    <div className="App">
      {/* {!realtimeFlag && !positionsFlag && (
        <TLSelector
          onPositionClick={setPositionsFlag}
          onRealtimeClick={setRealtimeFlag}
        />
      )} */}
      {/* {positionsFlag && <TLPositions />} */}
      <TLRealtimeStatus />
    </div>
  );
};
