export * from "./map";

export type WithChildren = {
  children?: React.ReactNode;
};
