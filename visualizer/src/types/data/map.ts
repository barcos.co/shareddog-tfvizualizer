type CoordinatePair = [number, number];
type RefPointType = "Point";

type IntersectionType = "v2x-map";

type PossibleManeuvers = "straight" | "unknown";

export type LaneType = "vehicle" | "pedestrian";

type IntersectionID = {
  intersectionId: number;
};

type Connection = {
  connectedLaneId: number;
  possibleManeuvers: PossibleManeuvers;
  signalGroupId: number;
};

type NodeType = "LineString";

export type LaneObjectType = {
  bearing_deg: number;
  connections: Array<Connection>;
  egressLane: boolean;
  ingressLane: boolean;
  laneId: number;
  laneType: LaneType;
  nodes: { coordinates: Array<CoordinatePair>; type: NodeType };
  possibleManeuvers: PossibleManeuvers;
};

export type MapMessage = IntersectionID & {
  laneWidth_m: number;
  lanes: Array<LaneObjectType>;
  refPoint: { coordinates: CoordinatePair; type: RefPointType };
  type: IntersectionType;
};

type GNSSProperty = {
  latitude: number;
  longitude: number;
  altitude_m: number;
  speed_mps: number;
  time_utc: number;
  heading_deg: number;
  acceleration_mpss: number;
  curvature: { radiusOfCurve_m: number; confidence: number };
  positionError_stdDev: {
    posErrSemiMajorAxisLen_m: number;
    posErrSemiMinorAxisLen_m: number;
    posErrSemiMajorAxisOrientation_deg: number;
    altitudeErr_m: number;
    headingErr_deg: number;
    groundSpeedErr_mps: number;
    verticalSpeedErr_mps: number;
    DOP: number;
  };
  satinfo: {
    fix: "3d";
    numSatUsed: number;
    numSatVisible: { gps: number; gbas: number; sbas: number; gnss: number };
  };
};

type GNSSPathHistory = {
  refLatitude: number;
  refLongitude: number;
  refAltitude: number;
  pathPoints: Array<{
    deltaLatitude: number;
    deltaLongitude: number;
    deltaAltitude_m: number;
    pathDeltaTime_ms: number;
  }>;
};

type GNSSOwnInfo = {
  stationID: number;
  stationType: number;
  vehicleRole: number;
  vehicleWidth_m: number;
  vehicleLength_m: number;
  yawRate_degps: number;
};

export type MovementStateType = "green" | "yellow" | "red" | "dark" | "unknown";

export type MovementStateEvent = {
  eventState: MovementStateType;
  likelyTime: string;
  maxEndTime: string;
  minEndTime: string;
};
export type MovementState = {
  signalGroupId: number;
  events: Array<MovementStateEvent>;
};

export type SpatMessage = IntersectionID & {
  movementStates: Array<MovementState>;
  type: "v2x-spat";
};

export type GNSSMessage = {
  obu_timestamp_sec: number;
  gnss: GNSSProperty;
  own_info: GNSSOwnInfo;
  repathHistoryfPoint: GNSSPathHistory;
};

export type MapData = Array<MapMessage>;
