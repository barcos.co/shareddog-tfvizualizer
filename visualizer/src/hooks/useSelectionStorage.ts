import { useState } from "react";
import { LaneType } from "../types/data";

export type SelectionData = {
  latLong: google.maps.LatLng | null;
  intersectionId: number;
  laneId: number;
  type: LaneType;
  signalGroupId: number;
  position: { lat: number; lng: number };
};

export interface IUseSelectionStorageHook
  extends ReturnType<typeof useSelectionStorage> {}

export const defaultSelectionStorage: IUseSelectionStorageHook = {
  selection: undefined,
  setSelection: () => undefined,
};

export const useSelectionStorage = () => {
  const [selection, setSelection] = useState<SelectionData | undefined>(
    undefined
  );

  return { selection, setSelection };
};
