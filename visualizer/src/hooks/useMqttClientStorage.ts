import { useEffect, useState } from "react";
import mqtt from "mqtt/dist/mqtt";

type MqttClientState = "offline" | "connected" | "connecting";
const LOCAL_STORAGE_LABEL = "sgdbrokeraddress";

export interface IUseMqttClientStorageHook
  extends ReturnType<typeof useMqttClientStorage> {}

export const defaultMqttClient: IUseMqttClientStorageHook = {
  mqttConnect: () => new Promise(() => null),
  mqttDisconnect: () => null,
  mqttSubscribe: () => new Promise(() => null),
  mqttUnSubscribe: () => new Promise(() => null),

  payload: { topic: "", message: undefined },
  data: [],

  mqttClient: undefined,

  connected: false,
  connecting: false,
  offline: true,
  setState: (some, value) => null,

  brokerAddress: "",
  updateBrokerValue: () => "",
};

export const useMqttClientStorage = () => {
  const [data, setData] = useState<Array<any>>([]);

  const [brokerAddress, setBrokerAddress] = useState("192.168.2.2");
  const [connected, setConnected] = useState(false);
  const [connecting, setConnecting] = useState(false);
  const [offline, setOffline] = useState(false);
  const [payload, setPayload] = useState({ topic: "", message: undefined });

  const [mqttClient, setMqttClient] = useState<undefined | mqtt.Client>(
    undefined
  );

  const setState = (state: MqttClientState, value: boolean) => {
    switch (state) {
      case "offline":
        setOffline(value);
        break;
      case "connected":
        setConnected(value);
        break;
      case "connecting":
        setConnecting(value);
        break;
    }
  };

  const updateBrokerValue = (event: React.ChangeEvent<HTMLInputElement>) => {
    const newValue = event.target.value;
    setBrokerAddress(newValue);
    localStorage.setItem(LOCAL_STORAGE_LABEL, newValue);
  };

  // get the current value stored for the broker
  useEffect(() => {
    const savedValue = localStorage.getItem(LOCAL_STORAGE_LABEL);
    if (savedValue) {
      setBrokerAddress(savedValue);
    }
  }, []);

  useEffect(() => {
    mqttConnect();
    return () => {
      if (mqttClient) {
        mqttClient.end();
      }
    };
  }, []);

  useEffect(() => {
    if (mqttClient) {
      mqttClient.on("connect", () => {
        console.debug("Connected to MQTT broker");
        setState("connected", true);
        setState("connecting", false);
        setState("offline", false);
      });

      // mqttClient.on("message", onMessage);

      mqttClient.on("offline", () => {
        // Handle offline event
        setState("connected", false);
        setState("connecting", false);
        setState("offline", true);
      });

      // client.on('connect', () => {
      //   setIsConnected(true);
      //   console.log('MQTT Connected');
      // });
      // client.on('error', (err) => {
      //   console.error('MQTT Connection error: ', err);
      //   client.end();
      // });

      mqttClient.on("error", (error: Error) => {
        // setConnected(false);
        mqttClient.end();
      });

      mqttClient.on("message", (_topic: string, buffer: Buffer) => {
        const parsedMessage = JSON.parse(buffer.toString());
        const payloadMessage = {
          topic: _topic,
          message: parsedMessage,
          timestamp: new Date().getTime(),
        };
        console.debug(payloadMessage);
        setData((prevData) => [...prevData, payloadMessage]);
        setPayload(payloadMessage);
      });
    }
  }, [mqttClient]);

  const mqttConnect = async () => {
    try {
      setState("connecting", true);
      const client = mqtt.connect(`ws://${brokerAddress}`, {
        protocol: "ws",
        port: 9001,
      });

      setMqttClient(client);
    } catch (error) {
      setState("connected", false);
      setState("connecting", false);
      setState("offline", false);
      console.error((error as Error)?.message ?? error);
    }
  };

  const mqttDisconnect = () => {
    if (mqttClient) {
      mqttClient.removeAllListeners();
      mqttClient.end(false, {}, () => {
        console.log("MQTT Disconnected");
        setState("connected", false);
        setState("connecting", false);
        setState("offline", true);
      });
    }
  };

  const mqttSubscribe = async (topic: string) => {
    if (mqttClient) {
      console.log("MQTT subscribe ", topic);
      const clientMqtt = await mqttClient.subscribe(
        topic,
        { qos: 0, rap: false, rh: 0 },
        (error: unknown) => {
          if (error) {
            console.log("MQTT Subscribe to topics error", error);
            return;
          }
        }
      );
      setMqttClient(clientMqtt);
    }
  };

  const mqttUnSubscribe = async (topic: string) => {
    if (mqttClient) {
      const clientMqtt = await mqttClient.unsubscribe(topic, (error: Error) => {
        if (error as unknown) {
          console.log("MQTT Unsubscribe error", error);
          return;
        }
      });
      setMqttClient(clientMqtt);
    }
  };

  return {
    mqttConnect,
    mqttDisconnect,
    mqttSubscribe,
    mqttUnSubscribe,

    payload,

    mqttClient,
    data,

    offline,
    connected,
    connecting,
    setState,

    brokerAddress,
    updateBrokerValue,
  };
};
