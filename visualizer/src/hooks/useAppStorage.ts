import { useState } from "react";
import {
  IUseSelectionStorageHook,
  useSelectionStorage,
} from "./useSelectionStorage";
import {
  IUseMqttClientStorageHook,
  useMqttClientStorage,
} from "./useMqttClientStorage";
import { IUseV2XStorageHook, useV2XStorage } from "./useV2XStorage";

export type UseAppStoreHook = {
  apiKey: string;
  setApiKey: React.Dispatch<React.SetStateAction<string>>;

  realtimeFlag: boolean;
  setRealtimeFlag: React.Dispatch<React.SetStateAction<boolean>>;
  positionsFlag: boolean;

  setPositionsFlag: React.Dispatch<React.SetStateAction<boolean>>;

  realtime: {
    v2xStorage: IUseV2XStorageHook;
    selectionStorage: IUseSelectionStorageHook;
    mqttClientStorage: IUseMqttClientStorageHook;
  };
};

export const useAppStorage = (): UseAppStoreHook => {
  const [apiKey, setApiKey] = useState<string>("");
  const [realtimeFlag, setRealtimeFlag] = useState<boolean>(false);
  const [positionsFlag, setPositionsFlag] = useState<boolean>(false);

  const v2xStorage = useV2XStorage();
  const selectionStorage = useSelectionStorage();
  const mqttClientStorage = useMqttClientStorage();

  return {
    apiKey,
    setApiKey,

    realtimeFlag,
    setRealtimeFlag,

    positionsFlag,
    setPositionsFlag,

    realtime: {
      v2xStorage,
      selectionStorage,
      mqttClientStorage,
    },
  };
};
