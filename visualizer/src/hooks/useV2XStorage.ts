import { useState } from "react";
import { GNSSMessage, MapMessage, SpatMessage } from "../types/data";
import { SelectionData } from "./useSelectionStorage";

type V2XMapData = Map<number, MapMessage>;
type V2XGNSSData = GNSSMessage;
type V2XSpatData = Map<number, SpatMessage>;

const SPAT_TOPIC = "v2x-uca/output/json/spat";
const MAP_TOPIC = "v2x-uca/output/json/map";
const GNSS_TOPIC = "v2x/rx/obu_gnss";

export const Topics = {
  SPAT: SPAT_TOPIC,
  MAP: MAP_TOPIC,
  GNSS: GNSS_TOPIC,
};

export interface IUseV2XStorageHook extends ReturnType<typeof useV2XStorage> {}

export const defaultV2xStorage: IUseV2XStorageHook = {
  mapDataUpdatedAt: new Date(),
  mapData: new Map(),
  setMapData: () => null,

  gnssDataUpdatedAt: new Date(),
  gnssData: undefined,
  setGNSSData: () => null,

  spatDataUpdatedAt: new Date(),
  spatData: new Map(),
  setSpatData: () => null,
};

export const useV2XStorage = () => {
  const [mapData, setMap] = useState<V2XMapData>(new Map());
  const [mapDataUpdatedAt, setMapDataUpdatedAt] = useState<Date>(new Date());

  const [gnssData, setGnss] = useState<V2XGNSSData | undefined>(undefined);
  const [gnssDataUpdatedAt, setGnssDataUpdatedAt] = useState<Date>(new Date());

  const [spatData, setSpat] = useState<V2XSpatData>(new Map());
  const [spatDataUpdatedAt, setSpatDataUpdatedAt] = useState<Date>(new Date());

  const setMapData = (data: MapMessage, someSelection?: SelectionData) => {
    const { intersectionId } = data ?? {};
    if (typeof intersectionId === "number") {
      // filter all lanes that match to pedestrians
      data.lanes = data.lanes.filter((lane) => lane.laneType === "pedestrian");

      const possibleErrors = data.lanes.filter(
        (lane) => lane.nodes.coordinates.length > 2
      );

      if (possibleErrors.length) {
        console.warn(possibleErrors);
        const prefix = `usePedetriansV2X: intersection ${intersectionId}`;
        console.warn(`${prefix} - Possible error on function setMapData`);
        console.warn(
          `${prefix} - Number of lanes with more than 2 coordinates set. ${possibleErrors.length}`
        );
        possibleErrors.forEach((error) => {
          if (error) {
            console.warn(
              `${prefix} - laneId:${error.laneId} Nr or coordinates: ${error.nodes.coordinates.length}`
            );
          }
        });
      }

      try {
        const prefix = `setMapData:::  intersectionId:${intersectionId}`;
        if (intersectionId === someSelection?.intersectionId) {
          const lane = data.lanes.find(
            (lane) => lane.laneId === someSelection.laneId
          );
          if (lane) {
            const group = lane.connections.find(
              (connection) =>
                connection.signalGroupId === someSelection.signalGroupId
            );
            if (group) {
              console.debug(`${prefix} signalGroupId:${group.signalGroupId}`);
            } else
              throw new Error(
                `${prefix} signalGroupId:${someSelection.signalGroupId} not found`
              );
          } else
            throw new Error(
              `${prefix} laneId:${someSelection.laneId} not found`
            );
        } // else throw new Error("no selection");
      } catch (error) {
        console.error(error);
      }

      // update and insert if not exsist
      setMap(mapData.set(intersectionId, data));
      // console.debug(`map intesections count ${mapData.size}`);
      setMapDataUpdatedAt(new Date());
    }
  };

  const setGNSSData = (data: GNSSMessage) => {
    setGnss(data);
    setGnssDataUpdatedAt(new Date());
  };

  const setSpatData = (data: SpatMessage, someSelection?: SelectionData) => {
    const { intersectionId } = data ?? {};
    if (typeof intersectionId === "number") {
      // update and insert if not exsist

      const currentMapData = mapData.get(intersectionId);
      if (currentMapData) {
        const validsignalGroupIds: Array<number> = [];

        currentMapData.lanes.forEach((lane) =>
          lane.connections.forEach((connection) =>
            validsignalGroupIds.push(connection.signalGroupId)
          )
        );

        // filter all movementStates that belongs to pedestrians
        data.movementStates = data.movementStates.filter((state) => {
          let shouldAdd = false;
          if (validsignalGroupIds.find((id) => id === state.signalGroupId)) {
            shouldAdd = true;
          }
          return shouldAdd;
        });

        try {
          const prefix = `setSpatData::: intersectionId:${intersectionId}`;
          if (intersectionId === someSelection?.intersectionId) {
            const state = data.movementStates.find(
              (state) => state.signalGroupId === someSelection.signalGroupId
            );
            if (state) {
              const eventState = state.events[0].eventState;
              console.debug(
                `${prefix} signalGroupId:${someSelection.signalGroupId} state:${eventState}`
              );
            } else
              throw new Error(
                `${prefix} signalGroupId:${someSelection.signalGroupId} not found`
              );
          } //else throw new Error("no selection");
        } catch (error) {
          console.error(error);
        }

        setSpat(spatData.set(intersectionId, data));
        // console.debug(`spat events count ${spatData.size}`);
        setSpatDataUpdatedAt(new Date());
      }
    }
  };

  return {
    mapDataUpdatedAt,
    mapData,
    setMapData,

    gnssDataUpdatedAt,
    gnssData,
    setGNSSData,

    spatDataUpdatedAt,
    spatData,
    setSpatData,
  };
};
