import React from "react";
import AppHeader from "../components/AppHeader";

type TLSelectorProps = {
  onPositionClick: React.Dispatch<React.SetStateAction<boolean>>;
  onRealtimeClick: React.Dispatch<React.SetStateAction<boolean>>;
};

export const TLSelector = (props: TLSelectorProps) => {
  const { onPositionClick, onRealtimeClick } = props;
  return (
    <>
      <AppHeader title="🚦 SGD Traffic Lights Selector"></AppHeader>
      <h2>Please click one option</h2>
      <div className="selector-container">
        <div className="selector-box" onClick={() => onPositionClick(true)}>
          <h1>TL Positions</h1>
        </div>
        <div className="selector-box" onClick={() => onRealtimeClick(true)}>
          <h1>TL Realtime</h1>
        </div>
      </div>
    </>
  );
};
