import React from "react";
import GoogleMaps from "../components/realtime/GoogleMaps";
import MQTTBrokerConfigurator from "../components/MQTTBrokerConfigurator";
import AppHeader from "../components/AppHeader";

const TLRealtimeStatus = () => {
  return (
    <div>
      <AppHeader title="🚦 SGD Traffic Lights Realtime">
        <MQTTBrokerConfigurator></MQTTBrokerConfigurator>
      </AppHeader>
      <GoogleMaps />
    </div>
  );
};

export default TLRealtimeStatus;
