import React, { useContext, useState, useEffect } from "react";
// import { LaneObjectType, MapData } from "./types/data";
import GoogleMaps from "../components/positions/GoogleMaps";
import "react-toastify/dist/ReactToastify.css";
import { ToastContainer } from "react-toastify";
import { toast } from "react-toastify";
import { TLPositionContext } from "../contexts/positions";
import AppHeader from "../components/AppHeader";

export const TLPositions = () => {
  const {
    mapData,
    simplifiedMapData,
    pedestrians,
    apiKey: googleMapsApiKey,
  } = useContext(TLPositionContext);

  const total = mapData.length;
  const simplifiedTotal = simplifiedMapData?.length ?? 0;

  const inputRef = React.createRef<HTMLInputElement>();
  const [someKey, setApiKey] = useState("");

  useEffect(() => {
    console.warn("setting new google api key -> ", googleMapsApiKey);
    setApiKey(googleMapsApiKey);
  }, []);

  useEffect(() => {
    if (!googleMapsApiKey && !someKey) {
      toast.error(
        <div>
          <input ref={inputRef} type="text" placeholder="Google API key" />
          <button
            onClick={() => {
              toast.dismiss();
              setApiKey(inputRef.current?.value ?? "");
            }}
          >
            Submit
          </button>
        </div>,
        { closeButton: false, autoClose: false }
      );
    }
  }, [googleMapsApiKey, someKey, setApiKey, inputRef]);

  return (
    <>
      <AppHeader
        title="🚦 SGD Traffic Lights Position"
        // childrenLeft={
        //   <p>
        //     Unique intersection entries: <b>{simplifiedTotal}</b>
        //     <br></br>
        //     Pedestrians intersection entries: <b>{pedestrians?.length ?? 0}</b>
        //     <br></br>
        //     Collected intersection entries: <b>{total}</b>
        //     <br></br>
        //   </p>
        // }
      ></AppHeader>

      {someKey !== "" ? (
        <GoogleMaps
          googleMapsApiKey={someKey}
          intersections={pedestrians?.map((item) => ({
            id: item.intersectionId,
            trafficLights: item.lanes.map((lane) => ({
              id: lane.laneId,
              position: lane.nodes.coordinates?.map((coordinate) => ({
                lng: coordinate[0],
                lat: coordinate[1],
              })),
            })),
          }))}
        />
      ) : (
        <h1 style={{ marginTop: "200px" }}>
          Please enter your Google Maps API key first
        </h1>
      )}
      <ToastContainer
        position="top-right"
        // autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick={false}
        rtl={false}
        // pauseOnFocusLoss
        draggable={false}
        pauseOnHover
        theme="light"
      />
    </>
  );
};
