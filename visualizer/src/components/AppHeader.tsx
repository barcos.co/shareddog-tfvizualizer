import React from "react";
import hawLogo from "../icons/haw.svg";

type HeaderProps = {
  title: string;
  children?: React.ReactNode;
};

const AppHeader = (props: HeaderProps) => {
  const { children, title } = props;
  return (
    <header className="App-header">
      <div
        style={{
          textAlign: "left",
          marginLeft: "25px",
          color: "white",
          fontSize: "1.2rem",
          position: "absolute",
          left: 0,
        }}
      >
        {children}
      </div>
      <h2>{title}</h2>
      <img
        src={hawLogo}
        width="140px"
        className="haw-logo "
        alt="haw-logo"
      ></img>
    </header>
  );
};

export default AppHeader;
