import React, { useContext, useEffect } from "react";
import { Topics } from "../hooks";
import { AppStorageContext } from "../contexts";
import { GNSSMessage, MapMessage, SpatMessage } from "../types/data";

type MQTTBrokerConfiguratorProps = {
  children?: React.ReactNode;
};

const MQTTBrokerConfigurator = (props: MQTTBrokerConfiguratorProps) => {
  const {
    realtime: { mqttClientStorage, v2xStorage, selectionStorage },
  } = useContext(AppStorageContext);

  const { selection } = selectionStorage;

  const { mqttConnect, mqttDisconnect, mqttSubscribe } = mqttClientStorage;
  const { brokerAddress, updateBrokerValue } = mqttClientStorage;
  const { offline, connected, connecting, payload, data } = mqttClientStorage;

  const { setMapData, setGNSSData, setSpatData } = v2xStorage;

  useEffect(() => {
    if (connected) {
      mqttSubscribe(Topics.GNSS);
      mqttSubscribe(Topics.MAP);
      mqttSubscribe(Topics.SPAT);
    }
  }, [connected]);

  useEffect(() => {
    const { topic, message } = payload;
    if (topic && message) {
      if (topic === Topics.MAP) setMapData(message as MapMessage, selection);
      if (topic === Topics.GNSS) setGNSSData(message as GNSSMessage);
      if (topic === Topics.SPAT) setSpatData(message as SpatMessage, selection);
    }
  }, [payload, selection]);

  const downloadData = () => {
    const blob = new Blob([JSON.stringify(data)], { type: "application/json" });
    const url = URL.createObjectURL(blob);
    const a = document.createElement("a");
    a.href = url;
    a.download = "data.json";
    a.click();
    URL.revokeObjectURL(url);
  };

  return (
    <>
      <h3 style={{ margin: "0px", marginBottom: "5px" }}>
        MQTT Broker Configurator
      </h3>
      <input
        type="url"
        placeholder="Broker Host"
        onChange={updateBrokerValue}
        disabled={connecting}
        value={brokerAddress}
        style={{ marginRight: "10px" }}
      ></input>
      <button
        disabled={brokerAddress === "" || connecting}
        onClick={() => (connected ? mqttDisconnect() : mqttConnect())}
      >
        {connected || (offline && !connecting)
          ? "restart connection"
          : connecting
          ? "connecting..."
          : "connect"}
      </button>
      <button onClick={() => (connected ? downloadData() : undefined)}>
        Download
      </button>

      <div
        style={{
          color: offline ? "black" : "white",
          backgroundColor: connected ? "green" : offline ? "yellow" : "red",
          marginTop: "10px",
          fontSize: "10px",
          width: "fit-content",
          padding: "4px",
          paddingRight: "15px",
          paddingLeft: "15px",
          borderRadius: "10px",
        }}
      >
        {connected ? "Connected" : offline ? "Offline" : "Disconnected"}
      </div>
    </>
  );
};

export default MQTTBrokerConfigurator;
