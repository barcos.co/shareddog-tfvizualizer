import React from "react";
import { MovementStateType } from "../../types/data";

type Props = {
  currentState: MovementStateType;
};

const TrafficLight = (props: Props) => {
  const { currentState } = props;

  return (
    <div className="traffic-light">
      <div
        className={`light ${
          currentState === "red" ? "active-red blinking-circle" : ""
        }`}
      ></div>
      <div
        className={`light ${
          currentState === "green" ? "active-green blinking-circle" : ""
        }`}
      ></div>
    </div>
  );
};

export default TrafficLight;
