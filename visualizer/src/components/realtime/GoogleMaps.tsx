import React, { useContext, useEffect, useState } from "react";
import { DevelopedByLabel } from "../DevelopedByLabel";
import { GoogleMap, useJsApiLoader, Marker } from "@react-google-maps/api";
import position from "../../icons/position3.png";
import tlIcon from "../../icons/ampel.png";
import { MovementStateEvent } from "../../types/data";
import { RealtimeGoogleMapsMarkerInfo } from "./GoogleMapsMarkerInfo";
import { AppStorageContext } from "../../contexts";
import { SelectionData } from "../../hooks";

const containerStyle = {
  display: "grid",
  width: "100%",
  height: "88vh",
};

const GoogleMaps = () => {
  const [firstCurrent, setFirstCurrent] = useState<
    { lat: number; lng: number } | undefined
  >(undefined);

  const {
    apiKey,
    realtime: { v2xStorage, selectionStorage },
  } = useContext(AppStorageContext);

  const { selection, setSelection } = selectionStorage;

  const { gnssData, mapData, spatData } = v2xStorage;
  const mapRef = React.createRef<GoogleMap>();

  const { isLoaded } = useJsApiLoader({
    id: "google-map-script-realtime",
    googleMapsApiKey: apiKey,
  });

  const onMapLoad = (map: google.maps.Map) => {
    //     const bounds = new window.google.maps.LatLngBounds(center);
    //     map.fitBounds(bounds);
  };

  useEffect(() => {
    if (!firstCurrent && gnssData?.gnss) {
      const { latitude, longitude } = gnssData?.gnss;
      setFirstCurrent({ lat: latitude, lng: longitude });
    }
  }, [firstCurrent, gnssData?.gnss]);

  const onMarkerClick = (
    event: google.maps.MapMouseEvent,
    payload: SelectionData
  ) => {
    const { intersectionId, laneId, type, signalGroupId, position, latLong } =
      payload;

    setSelection({
      intersectionId,
      laneId,
      type,
      signalGroupId,
      latLong,
      position: { lat: position.lat, lng: position.lng },
    });
  };

  if (isLoaded && gnssData?.gnss) {
    const {
      gnss: { latitude, longitude },
    } = gnssData;

    const currentPosition = { lat: latitude, lng: longitude };

    return (
      <GoogleMap
        ref={mapRef}
        mapContainerStyle={containerStyle}
        // center={currentPosition}
        center={firstCurrent}
        mapTypeId="satellite"
        zoom={19}
        onLoad={onMapLoad}
      >
        <DevelopedByLabel />
        <Marker
          key={`current-position`}
          position={currentPosition}
          icon={position}
        />
        {Array.from(spatData).map(([intersectionId, { movementStates }]) => {
          const markers: Array<{
            intersectionId: number;
            position: { lat: number; lng: number };
            movementStates: Array<MovementStateEvent>;
            signalGroupId: number;
            laneId: number;
          }> = [];

          const signalGroupId = movementStates.map(
            (state) => state.signalGroupId
          );

          Array.from(mapData).map(([intersectionId, { lanes }]) => {
            lanes
              .filter((lane) => lane.laneType === "pedestrian")
              .map((lane) =>
                lane.connections.map((connection) => {
                  if (signalGroupId.includes(connection.signalGroupId)) {
                    // TODO while rendering the lane.nodes.coordinates could have more than 2 CoordinatePairs
                    // another aproach should be found

                    const coordinate = lane.nodes.coordinates[0];
                    const lng = coordinate[0];
                    const lat = coordinate[1];
                    const states =
                      movementStates.find(
                        (state) =>
                          state.signalGroupId === connection.signalGroupId
                      )?.events ?? [];

                    markers.push({
                      intersectionId,
                      position: { lat, lng },
                      movementStates: states,
                      signalGroupId: connection.signalGroupId,
                      laneId: lane.laneId,
                    });
                  }
                })
              );
          });

          return markers.map((marker) => {
            const { signalGroupId, position, laneId } = marker;
            return (
              <Marker
                key={`signalGroupId-${signalGroupId}-${laneId}`}
                position={position}
                icon={tlIcon}
                onClick={(event) =>
                  onMarkerClick(event, {
                    ...marker,
                    type: "pedestrian",
                    latLong: event.latLng,
                    intersectionId,
                  })
                }
              />
            );
          });
        })}
        <RealtimeGoogleMapsMarkerInfo />
      </GoogleMap>
    );
  }
  return null;
};

export default React.memo(GoogleMaps);
