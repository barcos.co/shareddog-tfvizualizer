import React, { useContext, useEffect, useState } from "react";
import { LaneType, MovementStateType } from "../../types/data";
import { CloseIcon } from "../CloseIcon";
import TrafficLight from "./TrafficLight";
import { AppStorageContext } from "../../contexts";

const MarkerInfoHeader = (props: { type?: LaneType }) => {
  const { type } = props;
  return (
    <div
      style={{
        padding: "20px",
        backgroundColor: "#282c34",
        color: "white",
      }}
    >
      <h2 style={{ fontSize: "80px", padding: "0px", margin: "0px" }}>
        🚦
        {type === "pedestrian" ? "👩‍🦯" : type === "vehicle" ? "🚗" : "❔"}
      </h2>
      <h1 style={{ marginLeft: "20px" }}>Marker Information</h1>
    </div>
  );
};

export const RealtimeGoogleMapsMarkerInfo = () => {
  const {
    realtime: { selectionStorage, v2xStorage },
  } = useContext(AppStorageContext);

  const { selection, setSelection } = selectionStorage;
  const { spatData, spatDataUpdatedAt } = v2xStorage;

  const [isVisible, setIsVisible] = useState(false);
  const [currentState, setCurrentState] =
    useState<MovementStateType>("unknown");

  const { type, laneId, intersectionId, latLong, signalGroupId } =
    selection ?? {};

  useEffect(() => {
    setIsVisible(Boolean(intersectionId));
  }, [intersectionId]);

  useEffect(() => {
    if (typeof intersectionId === "number") {
      const spatMessage = spatData.get(intersectionId);
      if (spatMessage) {
        const movementState = spatMessage.movementStates.find(
          (state) => state.signalGroupId === signalGroupId
        );
        if (movementState) {
          const eventState = movementState.events[0].eventState;
          setCurrentState(eventState);
        }
      }
    }
  }, [spatDataUpdatedAt, spatData, intersectionId, signalGroupId]);

  if (!selection) return null;

  return (
    <div
      className={`myVisibleDiv ${isVisible ? "visible" : ""}`}
      style={{
        textAlign: "left",
        position: "absolute",
        backgroundColor: "white",
        width: "500px",
        boxShadow: "15px 30px 10px #00000066",
        bottom: 0,
      }}
    >
      <MarkerInfoHeader type={type} />
      <div
        style={{ position: "relative", padding: "20px", paddingLeft: "40px" }}
      >
        <h1 style={{ margin: "0px" }}>Lane ID: {laneId}</h1>
        <h2 style={{ margin: "0px" }}>Intersection ID: {intersectionId}</h2>
        <h3 style={{ margin: "0px" }}>SignalGroup ID: {signalGroupId}</h3>

        <h3>
          Latitude:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{latLong?.lat()}
          <br></br>
          Longitude:&nbsp;&nbsp;{latLong?.lng()}
        </h3>
        <TrafficLight
          // remainingTime={remainTime}
          currentState={currentState}
        />
        <p style={{ fontSize: "10px", textAlign: "end" }}>
          {latLong?.lat()} , {latLong?.lng()}
        </p>
      </div>

      <CloseIcon onClick={() => setSelection(undefined)} />
    </div>
  );
};
