# 🐶 Shared Guide Dog 4.0 - 👩‍🦯 Traffic Lights 🚦 - Studentwork

This folder inside the Shared Guide Dog 4.0 project is a sub-repository created for the purpose of a study project in the field of Mechatronics at the Hamburg University of Applied Sciences. The project focuses on the detection and recognition of traffic signals using the v2x protokoll and based on ros2 packages written in python

The main goal of the project is to develop a system that can accurately identify different types of traffic signals such as traffic lights, lane types, and more. The system will be designed to be integrated into a robotic guide dog to assist visually impaired individuals in safely navigating urban environments.

<br />
Created by: Avci, <a href="https://barcos.co">Barcos Paso</a>, Petersen
<br />

<br>
    <div align="center" justify="center">
        <img src="img/ampel.png" align="center" width="100%" alt="Realtime Chat" />
    </div>
<br />

<br>
    <div align="center" justify="center">
        <img src="img/maps.png" align="center" width="100%" alt="Realtime Chat" />
    </div>
<br />

# Requirements 🎯

The system was developed independently of the entire Shared Guide Dog system. It was necessary to keep the hardware and software installation as simple as possible.

## 🔩 Hardware

1. The Onboard Unit, also known as `mk5` or `obu` supplied by "consider it."
2. Battery or any voltage source (see the `obu` for more information about the voltage requirements).
3. An Ethernet Cable, preferably `Cat5` or higher, for connecting the `obu` to a computer or laptop.
4. (Optional) A switch for connecting the `obu` to more than one computer or laptop.

Keep in mind that the common case will involve a direct connection between the `obu` and a computer or laptop. This will disable the internet connection on the computer or laptop. If an internet connection is desired, two important things are needed. Firstly, a regular router with internet connection between the `obu` and the computer or laptop. Secondly, a set of configurations within the `obu` itself. For more information about this topic, please refer to the technical documentation.

## 🧑‍💻 Software

### Core system. (ROS Packages)

1.  ROS Foxy
2.  Python `v3.10` https://www.python.org/downloads/release/python-3100/
3.  Python library `paho-mqtt` `v1.6.1`. https://pypi.org/project/paho-mqtt/
4.  Packages under `./ros2/src`

### Recommended for development, visualization, and testing purposes.

1.  VSCode and VSCode Extensions recommendations published under `.vscode/extensions.json`
2.  Docker Engine `v23.0.5` and Docker Compose `v2.17.3`. On macOS, Docker Desktop version `4.19.0` is sufficient, as it includes the required versions of Docker Engine and Docker Compose. Windows users also could just install Desktop version `4.19.0` but `WSL 2` have to be installed. Docker Engine and Docker Compose could be installed on Linux distributions using any package managers like `apt`. https://docs.docker.com/desktop/

### Visualizer (Google Maps map)

1.  Docker Engine and Docker Compose (See recommendations)
2.  Google Maps API. In order to view the Google Maps map, an API Key will be required. You can add a `.env` file at the same level as the `docker-compose.yaml` file and add `REACT_APP_GOOGLE_API=some_generated_key`, or you can simply add the API Key as an environment variable within the `docker-compose.yaml` file

### Testing with mock data

1.  Docker Engine and Docker Compose (See recommendations)
2.  Jupyter notebook extension for VS Code (See recommendations)
3.  Python `v3.10` https://www.python.org/downloads/release/python-3100/

### Note 1

If you only desire the integration of the traffic light communication system with the current Shared Guide Dog 4.0 system, then only the requirements mentioned under &nbsp; `1. Core system (ROS Packages)` &nbsp;will be necessary.

### Note 2

While Docker Engine and Docker Compose are not required for the Shared Guide Dog 4.0 system, if the entire system is not available, a preconfigured Docker image of ROS2 Foxy is included within this sub-repository. Docker can help reduce the configuration time for a virtual machine where ROS2 Foxy needs to be installed. Docker will also be necessary for the Visualizer and Node-RED.

<br />
<br />

# Introduction 🥁

<div align="center" justify="center">
<img src="img/ampel2.png" align="center" width="100%" alt="Realtime Chat">
</div>

## System components

This repo has 4 important parts.

1. There are three `ROS2` packages that can be deployed or adjusted in a `production` environment. The first one is called `sgd_tl_interfaces`, which contains the main interfaces used by the communication system. The second one called `sgd_tl_action_client` that serves as example for the real implementetion of one client written in c++. And the third one package is named `sgd_tl_action_server` and serves as the primary package housing the main action server. It implements some Python classes for connecting to the real MQTT broker initiated by the MK5 and proccess a route request sent by any ROS2 client. This `sgd_tl_action_server` tracks the current location of the system and maintains data structures of the JSON data streamed by the traffic lights through the V2X protocol for sending feedbacks about the current traffic lights status to the client after receiving a request with a given set of LatLong data points.

2. A Jupiter Notebook (located in `notebooks/dummy_broker`), that starts an `MQTT broker` and a `dummy client` that publishes `dummy data` to the main `V2X topics` on localhost. This Jupiter could be used to mock up a broker and a published sending V2X datasets to different V2X topics.

3. The `Node-RED` application, deployed as a `Docker` container for processing real-time test data. This component is crucial for the entire system as it enables us to connect to an `MQTT broker` (either real or dummy), and convert raw data into a valid JSON file for later analysis or visualization. This is important because the traffic lights stream data through the `MQTT broker`. This data needs to be saved in a readable JSON format. The `Node-RED` application consists of two flows: a `Test Flow` and a `Production Flow`. The `Test Flow` is used for collecting test data through the `Node-RED` app, which is then processed and stored in a valid JSON file. The main difference between the `Test Flow` and the `Production Flow` is that the latter is designed to connect to the actual MQTT Broker of the MK5.

4. The `Visualizer` as React application, deployed as a Docker container for the `Visualizer`, which allows viewing the collected map data. Specifically, it focuses on displaying the locations of the traffic lights that have been gathered by the MK5.Please keep in mind that two things are required. Firstly, you need a valid `Google Maps API key`. Secondly, you will need a valid JSON file processed by `Node-RED` because the MK5 stores the messages in a JSON file with an invalid format for the `Visualizer`. The `Visualizer` can also show the current status of the traffic lights if a connection to the `MQTT broker` of the MK5 is established.

## Limitations of the system components 🐛

1. The Jupyter Notebook will run indefinitely until it is stopped. If all dummy data is consumed or streamed to the broker, you will need to stop the client (perform `client clean up`), and then rerun the two cells labeled with the following comment: `IMPORTANT: Rerun if needed (perform client clean up before)`. This client clean up need to be run every 15-20 minutes.

2. The Jupyter Notebook doesnt stop the MQTT Broker itself. Please run the `MQTT Broker clean up` (Last cell).

3. The `Node-RED` application does not create a new output file based on the day. In some cases, the JSON files created from the collected data may be larger than expected or get longer after every write operation. This can be a limitation as the other components will require more and more time for reading these files. One solution could be to move the json files (under `visualizer/src/data/raw` or `visualizer/src/data/processed`) to another folder every time that data get collected. since `Node-RED` wont be a production ready system component is not neccesesary to improve or work on this limitation.

4. The `Node-RED` container is not able to update the Flows and apply changes during its execution. This is becouse of docker. In order to update and apply the new updated Flow, you will first need to update the flow in `Node-RED`, copy the JSON representation of all flows, paste it inside the file flows.json located under `nodered/flows.json` and restart the `Node-RED` container.

5. The `Visualizer` will read the specific file called `map.json`, located under `visualizer/src/data/processed`. This file may be larger than expected. Therefore, it is important to handle it in a similar manner as the raw files, by moving it if necessary.

<br />
<br />

# How to start 🦮

## Testing the visualizer

Since almost all components will be deployed to a docker container this is the recommeded way if the whole Shared Guide Dog 4.0 system is not present. If you want to see the `Visualizer` you will need to:

1. Install all needed softwarte requirements.
2. Start the MQTT broker implemented as Jupyter Notebook. Do not start the client defined in the notebook until `Node-RED` starts
3. Ensure that the folders `visualizer/src/data/raw` and `visualizer/src/data/processed` are present
4. Start `Node-RED` for collecting and proccesing test data. In a terminal session `docker compose --profile development up nodered`
5. Start the client defined in the notebook.
6. Wait 5 Minutes and stop the client and the `MQTT Broker` defined in the Jupyter Notebook.
7. A `map.json` file should be created under `visualizer/src/data/processed`.
8. Stop the `Node-RED` Docker container by typing `CTRL+C` in the terminal session created in step 3.
9. Add the google maps api key inside the `docker-compose.yaml`.
10. Start the `Visualizer` by running `docker compose --profile development up webdev`
11. Open http://localhost:3000 and select `TL Positions`

## Development

Any class of the main 3 ROS2 Package could be improved or updated. In ros folder or in provided docker container under `/home/ws`

1. Run `rosdep update`
2. Run `rosdep install -i --from-path src --rosdistro foxy -y`
3. Run `colcon build && source install/setup.bash`
4. Test it was successfull inspecting the action by runnung `ros2 interface show sgd_tl_interfaces/action/SGDTLPedestrianTrafficLight`

If you develop without any hot reload library or functionality, you will need perform `step 3` for building the code every time the source code get changed.

The action server could be started by running:

```bash
    MQTT_HOST=10.23.24.3 MQTT_PORT=1883 ros2 run sgd_tl_action_server sgd_tl_action_server --ros-args --log-level debug
```

Change the `MQTT_HOST` or `MQTT_PORT` if needed. Defaults are localhost and 1883. If you want to use the Jupyter Notebook as `MQTT_HOST` and you want to develop with docker, the variable `MQTT_HOST` should be set to `host.docker.internal`. Keep in mind that the command must be executed inside the docker conatiner. This is why VSCode and its recommened extensions are helpful. The recommended extensions will make you able to start VS Code within a remote container. All needed configurations are under `./devcontainer` https://code.visualstudio.com/docs/devcontainers/containers

The Docker container with the core packages could be started just by running the command `>Dev Containers: Rebuild and reopen in container` the first time from the `Command Palette` of VS Code. It should take some minutes until the container is configured The next times just by running `>Dev Containers: open folder in container`

## Real test with real traffic light 🐶

1. Ensure all hardware requirements
2. Start the `sgd_tl_action_server` either as docker container as we did before for development or in the real Shared Guide Dog 4.0 system after its integration
3. See the logs in a terminal session or just start the `Visualizer` by runnung `docker compose --profile development up webdev` an select `Realtime TL Status`

## Sending a goal to the action server

Send 1 point

```bash
    ros2 action send_goal pedestrian_traffic_light sgd_tl_interfaces/action/SGDTLPedestrianTrafficLight "{route: [{latitude: 12.12, longitude: 13.13}]}"
```

Send 2 points

```bash
    ros2 action send_goal --feedback pedestrian_traffic_light sgd_tl_interfaces/action/SGDTLPedestrianTrafficLight "{route: [{latitude: 12.12, longitude: 43.43}, {latitude: 13.13, longitude: 44.44}]}"
```

Send multiple points

```bash
    ros2 action send_goal --feedback pedestrian_traffic_light sgd_tl_interfaces/action/SGDTLPedestrianTrafficLight "{route: [{latitude: 53.5559309, longitude: 9.9772394}, {latitude: 53.5559558, longitude: 9.9771795}, {latitude: 53.5559807, longitude: 9.9771196}, {latitude: 53.5560056, longitude: 9.9770596}, {latitude: 53.5560305, longitude: 9.9769997}, {latitude: 53.5560554, longitude: 9.9769398}, {latitude: 53.5560778, longitude: 9.9769635}, {latitude: 53.5561001, longitude: 9.9769872}, {latitude: 53.556126, longitude: 9.9770146}, {latitude: 53.5561519, longitude: 9.977042}, {latitude: 53.5561826, longitude: 9.9770747}, {latitude: 53.5562133, longitude: 9.9771073}, {latitude: 53.5562336, longitude: 9.9771345}, {latitude: 53.5562573, longitude: 9.9771066}, {latitude: 53.556281, longitude: 9.9770786}, {latitude: 53.5563022, longitude: 9.9770227}]}"
```

<br />
<br />

# Relevant data/links 🦯

- ISO/TS 19091:2019
- ISO/TS 19091 2019-06
- https://www.car-2-car.org/about-c-its/c-its-faqs
- https://www.car-2-car.org/about-c-its/c-its-glossary
- https://www.car-2-car.org/documents/general-documents
- https://standards.iso.org/iso/ts/19091/ed-2/en/ISO-TS-19091-addgrp-C-2018.asn
- Karte für v2x https://geoportal-hamburg.de/geo-online/
- https://tavf.hamburg/wir-sind-tavf#c36

<br />
<br />

# Links for ROS 2

- Installation - https://docs.ros.org/en/foxy/How-To-Guides/Setup-ROS-2-with-VSCode-and-Docker-Container.html#install-remote-development-extension
- Video series about devcontainer extensions for VS Code - https://www.youtube.com/watch?v=61M2takIKl8&list=PLj6YeMhvp2S5G_X6ZyMc8gfXPMFPg3O31
- Create a new package https://docs.ros.org/en/foxy/Tutorials/Beginner-Client-Libraries/Creating-Your-First-ROS2-Package.html
- Writing a simple publisher and subscriber (Python) - https://docs.ros.org/en/foxy/Tutorials/Beginner-Client-Libraries/Writing-A-Simple-Py-Publisher-And-Subscriber.html
- Writing a simple service and client (Python) - https://docs.ros.org/en/foxy/Tutorials/Beginner-Client-Libraries/Writing-A-Simple-Py-Service-And-Client.html
- Add Srv and msg - https://docs.ros.org/en/foxy/Tutorials/Beginner-Client-Libraries/Custom-ROS2-Interfaces.html

<br />
<br />

# FAQs

## Why c++ libs can not be found in container?

Inside the container make sure you add file under `/home/ws/.vscode/c_cpp_properties.json` and add following configuration in order to link the .hpp files generated for the interfaces

    {
        "configurations": [
            {
                "browse": {
                    "databaseFilename": "${default}",
                    "limitSymbolsToIncludedHeaders": false
                },
                "includePath": [
                    "/usr/include/**",
                    "/opt/ros/foxy/include/**",
                    "/home/ws/src/sgd_tl_action_client/include/**",
                    "/home/ws/src/sgd_tl_interfaces/include/**",
                    "${workspaceFolder}/install/sgd_tl_interfaces/include",
                    "${workspaceFolder}/build/sgd_tl_interfaces/**"

                ],
                "name": "ROS",
                "intelliSenseMode": "gcc-arm64", # this property was set for an apple silicon. change this if needed to "${default}"
                "compilerPath": "/usr/bin/gcc",
                "cStandard": "gnu11",
                "cppStandard": "c++14"
            }
        ],
        "version": 4
    }
